package com.rakesh.alarmmanagerexample;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {

	final public static String ONE_TIME = "onetime";
	final public static String TAG = "myLogs";
	@Override
	public void onReceive(Context context, Intent intent) {
		 PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
         PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK|
					PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "YOUR TAG");
         //Acquire the lock
         wl.acquire();
        
 		// Start the activity where you can stop alarm
 		
         //(context.getApplicationContext()).
         //You can do the processing here update the widget/remote views.
         Log.d(TAG,"before Bundle");
         Bundle extras = intent.getExtras();
         StringBuilder msgStr = new StringBuilder();
         
         
         if(extras != null && extras.getBoolean(ONE_TIME, Boolean.FALSE)){
        	 
        	 
        	 msgStr.append("One time Timer : ");
         }
         Format formatter = new SimpleDateFormat("hh:mm:ss a");
         msgStr.append(formatter.format(new Date()));

         Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();
         Log.d(TAG,"before wl.release");
         //Release the lock
         wl.release();
         Log.d(TAG,"before intent");
         Intent i = new Intent(context, AlarmTurnOff.class);
  		 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
  		 i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
  		 Log.d(TAG,"before start activity");
  		 context.startActivity(i);
         
	}
	public void SetAlarm(Context context, long addtime)
    {
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After after 30 seconds
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + addtime, 1000 * 5 , pi); 
    }

    public void CancelAlarm(Context context)
    {
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
    
    public void setOnetimeTimer(Context context,long addtime){
    	AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+addtime, pi);
    }
    
    
    
}
