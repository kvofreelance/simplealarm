package com.rakesh.alarmmanagerexample;

import java.util.Random;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AlarmTurnOff extends Activity implements OnClickListener{

	public EditText edit1;
	public EditText edit2;
	public EditText answer;
	public TextView operation;
	public Button alarmoff;
	
	public final String TAG = "myLogs";
	
	public Random random = new Random();
	public MediaPlayer mp;
	
	private int number1,number2,iOperation,iAnswer;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "setContent");	
        setContentView(R.layout.activity_alarm_off);
        
        edit1 = (EditText)findViewById(R.id.editText1);
        edit2 = (EditText)findViewById(R.id.editText2);
        answer = (EditText)findViewById(R.id.editText3);
        
        operation = (TextView)findViewById(R.id.textView1);
        alarmoff = (Button)findViewById(R.id.button1);
        
        mp = new MediaPlayer().create(getBaseContext(), R.raw.alarm);
        mp.setLooping(true);
        mp.start();
        
        alarmoff.setOnClickListener(this);
        
        Log.d(TAG, "initial View");	
        iOperation = random.nextInt(3);
        switch(iOperation){
        case 0:
        	this.operation.setText("+");
        	number1 = random.nextInt(100);
        	number2 = random.nextInt(100);
        	SetEdit();
        	break;
        case 1:
        	this.operation.setText("-");
        	
        	number1 = random.nextInt(100);
        	number2 = random.nextInt(number1);
        	SetEdit();
        	break;
        case 2:
        	this.operation.setText("*");
       
        	number1 = random.nextInt(100);
        	number2 = random.nextInt(3);
        	SetEdit();
        	break;
        	
        
        }
        Log.d(TAG, "create done");	
    }
    
	public void SetEdit()
	{
		edit1.setText(String.valueOf(number1));
        edit2.setText(String.valueOf(number2));
	}
	
	public boolean CheckIsFormulaTrue(int answer)
	{
		switch(iOperation){
        case 0:
        	this.operation.setText("+");
        	if(number1 + number2 == answer)
        	{
        		return true;
        	}
        	return false;
        	
        case 1:
        	this.operation.setText("-");
        	if(number1 - number2 == answer)
        	{
        		return true;
        	}
        	return false;
        	
        case 2:
        	this.operation.setText("*");
        	if(number1 * number2 == answer)
        	{
        		return true;
        	}
        	return false;
        	
        
        }
		return false;
		
	}
	
    @Override
	protected void onStart() {
		super.onStart();
	}
    
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_widget_alarm_manager, menu);
        return true;
    }*/

	@Override
	public void onClick(View arg0) {
		try{
		switch(arg0.getId()){
		case R.id.button1:
			if(CheckIsFormulaTrue(Integer.parseInt(answer.getText().toString())))
			{
				mp.stop();
			//Toast.makeText(getBaseContext(), "ALARM OFF", Toast.LENGTH_LONG).show();	
			}
			else
			{
				Toast.makeText(getBaseContext(), number1 + operation.getText().toString() +number2 + " is not eauql "+answer.getText().toString(), Toast.LENGTH_LONG).show();	
				
			}
			break;
		
		}
		}
		catch(Exception e)
		{
			
			Toast.makeText(getBaseContext(), number1 + operation.getText().toString() +number2 + " is not eauql "+answer.getText().toString(), Toast.LENGTH_LONG).show();	
		}
		
	}
}
