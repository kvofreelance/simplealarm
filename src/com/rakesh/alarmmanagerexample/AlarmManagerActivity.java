package com.rakesh.alarmmanagerexample;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

public class AlarmManagerActivity extends Activity {

	public TimePicker tp;
	public DatePicker dp;
	public Date date = new Date();
	public static final String TAG = "myLogs";
	
	private AlarmManagerBroadcastReceiver alarm;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_manager);
        
        tp = (TimePicker)findViewById(R.id.timePicker1);
        tp.setIs24HourView(true);
        
        dp = (DatePicker)findViewById(R.id.datePicker1);
        dp.setCalendarViewShown(false);
        
        alarm = new AlarmManagerBroadcastReceiver();
    }
    
    @Override
	protected void onStart() {
		super.onStart();
	}

    public void startRepeatingTimer(View view) {
    	Context context = this.getApplicationContext();
    	if(alarm != null){
    		long addtime = getAlarmTime();
    		alarm.SetAlarm(context,addtime);
    	}else{
    		Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
    	}
    }
    
    public void cancelRepeatingTimer(View view){
    	Context context = this.getApplicationContext();
    	if(alarm != null){
    		alarm.CancelAlarm(context);
    	}else{
    		Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
    	}
    }
    
    public void onetimeTimer(View view){
    	Context context = this.getApplicationContext();
    	if(alarm != null){
    		long addtime = getAlarmTime();
    		if (addtime<0){
    		Toast.makeText(context, "ALARM TIME IS IN PAST", Toast.LENGTH_SHORT).show();
    		}else{
    		alarm.setOnetimeTimer(context,addtime);
    		Toast.makeText(context, "ADD TIME "+addtime, Toast.LENGTH_SHORT).show();
    		}
    	}else{
    		Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
    	}
    }
    
    @SuppressWarnings("deprecation")
	private long getAlarmTime()
    {
    	//date.getHours()
    	int current_day = date.getDay();
    	int alarm_day = dp.getDayOfMonth();
    	int current_month = date.getMonth();
    	int alarm_month = dp.getMonth();
    	int current_year = date.getYear();
    	int alarm_year = dp.getYear();
    	Log.d(TAG, String.valueOf(alarm_year));
    	int current_hour = date.getHours();
    	int alarm_hour = tp.getCurrentHour();
    	int current_minute = date.getMinutes();
    	int alarm_minute = tp.getCurrentMinute();
    	
    	
    	boolean isAlarm = true;
    	if(current_year>alarm_year){
    		isAlarm =false;
    		Log.d(TAG, "year"+String.valueOf(isAlarm));
    	} 
    	if (current_month > alarm_month) {
    		isAlarm = false;
    		Log.d(TAG,"month"+String.valueOf(isAlarm));
    	} 
    	if (current_day > alarm_day) {
    		isAlarm = false;
    		Log.d(TAG, "day"+String.valueOf(isAlarm));
    	} 
    	if (current_hour > alarm_hour) {
    		isAlarm = false;
    		Log.d(TAG, "hour"+String.valueOf(isAlarm));
    	} 
    	if (current_minute > alarm_minute) {
    		isAlarm = false;
    		Log.d(TAG, "minute"+String.valueOf(isAlarm));
    	}
    	Log.d(TAG, String.valueOf(isAlarm));
    	if (isAlarm) 
    	{
    		
    		Date current_date = new Date();
    		
			Date alarm_date = new Date(alarm_year-1900,alarm_month,alarm_day,alarm_hour,alarm_minute);
    		/*alarm_date.setDate(alarm_day);
    		alarm_date.setMonth(alarm_month);
    		alarm_date.setYear(alarm_year);*/
    		Log.d(TAG, String.valueOf(alarm_date.getYear()));
    		Log.d(TAG, String.valueOf(alarm_year));
    		/*alarm_date.setHours(tp.getCurrentHour());
    		alarm_date.setMinutes(tp.getCurrentMinute());*/
    		
    		Log.d(TAG, String.valueOf(alarm_date) + "   "+ String.valueOf(current_date));
    		long dayMiliseconds = alarm_date.getTime() - current_date.getTime();
    				
	    	return (dayMiliseconds);
    	}
    	else
    	{
    		return -1;
    	}
    	
    }
    
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_widget_alarm_manager, menu);
        return true;
    }

    
}
